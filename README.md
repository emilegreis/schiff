# schiff (2020)

Some fraktur ASCII FIGfont experiments...

![schiff](snapshots/specimen.png "schiff")

## Requirements

- figlet

## Usage

    figlet -f schiff.flf schiff

## License

SIL Open Font License v1.1.

