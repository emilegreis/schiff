<!DOCTYPE html>
<head>
<meta charset="utf-8" />
<style>

    * {
        margin: 0;
        padding: 0;
    }

    @page {
        size: A4;
        margin-left: 64px;
        margin-top: 64px;
        margin-right: 64px;
        margin-bottom: 64px;
    }

    @font-face {
        font-family: unifont;
        src: url(unifont-12.1.03.ttf);
    }
    
    html, body {
        width: 100%;
        height: 100%; 
        background-color: blue;
        background-color: black;
    }

    /*80*62*/
    pre {
        background-color: red;
        background-color: black;
        display: block;
        margin: auto;
        page-break-after: always;

        width: 640px;
        height: auto;
        color: white;
        font-size : 16px;
        line-height: 16px;
        font-family: unifont;
    }

</style>
</head>
<body>
<pre><?php include 'specimen-1.txt';?></pre>
<pre><?php include 'specimen-2.txt';?></pre>
</body>
</html>
